part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();
  static const listview = _Paths.listview;
  static const newsdetail = _Paths.newsdetail;
}

abstract class _Paths {
  _Paths._();
  static const listview = '/listnews';
  static const newsdetail = '/newsdetail';
}
