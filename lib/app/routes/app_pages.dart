import 'package:get/get.dart';

import '../modules/listnews/bindings/listnews_binding.dart';
import '../modules/listnews/views/listnews_view.dart';
import '../modules/newsdetail/bindings/newsdetail_binding.dart';
import '../modules/newsdetail/views/newsdetail_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const initial = Routes.listview;

  static final routes = [
    GetPage(
      name: _Paths.listview,
      page: () => ListnewsView(),
      binding: ListnewsBinding(),
    ),
    GetPage(
      name: _Paths.newsdetail,
      page: () => NewsdetailView(),
      binding: NewsdetailBinding(),
    ),
  ];
}
