import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/newsdetail_controller.dart';

class NewsdetailView extends GetView<NewsdetailController> {
  @override
  Widget build(BuildContext context) {
    var objetNewsDetails = Get.arguments;
    print("${objetNewsDetails}");
    return Scaffold(
      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          children: [
            const SizedBox(
              height: 30,
            ),
            //Text(objetNewsDetails.title ?? ''),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                IconButton(
                  onPressed: () {
                    Get.back();
                  },
                  icon: const Icon(
                    Icons.arrow_back_ios,
                    color: Colors.black,
                  ),
                ),
                const CircleAvatar(
                  backgroundImage: AssetImage(
                    'assets/images/nature.jpg',
                  ),
                  radius: 20,
                ),
              ],
            ),
            const SizedBox(
              height: 16,
            ),
            Row(
              children: [
                Container(
                  padding: EdgeInsets.all(8),
                  decoration: BoxDecoration(
                      color: Colors.red,
                      borderRadius: BorderRadius.circular(20)),
                  child: const Text(
                    "C",
                    style: TextStyle(
                        fontSize: 16,
                        color: Colors.white,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                const SizedBox(
                  width: 16,
                ),
                const Text(
                  '|',
                  style: TextStyle(fontSize: 25),
                ),
                const SizedBox(
                  width: 8,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: const [
                    Text(
                      "12 Feb 2020",
                    ),
                    Text(
                      "CNBC Media Channel",
                      style:
                          TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                    ),
                  ],
                )
              ],
            ),
            const SizedBox(
              height: 16,
            ),
            Text(
              objetNewsDetails.title ?? '',
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
            ),
            const SizedBox(
              height: 16,
            ),
            Text(
              objetNewsDetails.description ?? '',
              style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500),
            ),
            const SizedBox(
              height: 16,
            ),
            CachedNetworkImage(
              imageUrl: objetNewsDetails.urlToImage ?? '',
              height: 200,
              width: double.infinity,
              fit: BoxFit.cover,
              placeholder: (context, url) => const CircularProgressIndicator(),
              errorWidget: (context, url, error) => const Icon(
                Icons.image_not_supported,
              ),
            ),

            ElevatedButton(
                onPressed: () {
                  controller.inertDataBase(objetNewsDetails);
                },
                child: const Text("Dispaly SqlFlite data in SqlFlite")),
            ElevatedButton(
                onPressed: () {
                  controller.getSqlData;
                },
                child: const Text("Insert in SqlFlite data base")),
          ],
        ),
      ),
    );
  }
}

//  Source? source;
//   String? author;
//   String? title;
//   String? description;
//   String? url;
//   String? urlToImage;
//   DateTime? publishedAt;
//   String? content;