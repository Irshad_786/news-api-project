import 'package:get/get.dart';

import '../../../../util/database_helper.dart';
import '../../../model/news_model.dart';

class NewsdetailController extends GetxController {
  //TODO: Implement NewsdetailController
   DataBaseHelper? dataBaseHelper;
  final count = 0.obs;
  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
    dataBaseHelper = DataBaseHelper();
  }

  // in this method calling insertdata method from Helper class
  void inertDataBase(Article article) {
    
    dataBaseHelper?.insertData(article);
  }
  void getSqlData(){
   var data= dataBaseHelper?.getData();
   //print("${data![1] ?? []}");
   }

  @override
  void onClose() {}
  void increment() => count.value++;
}
