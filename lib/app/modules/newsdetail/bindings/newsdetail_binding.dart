import 'package:get/get.dart';

import '../controllers/newsdetail_controller.dart';

class NewsdetailBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<NewsdetailController>(
      () => NewsdetailController(),
    );
  }
}
