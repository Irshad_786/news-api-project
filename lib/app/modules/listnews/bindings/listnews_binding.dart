import 'package:get/get.dart';

import '../controllers/listnews_controller.dart';

class ListnewsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ListnewsController>(
      () => ListnewsController(),
    );
  }
}
