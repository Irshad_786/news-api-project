// ignore_for_file: unnecessary_const

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:news_api/app/modules/listnews/views/change_theme_widget.dart';
import '../../../routes/app_pages.dart';
import '../controllers/listnews_controller.dart';

class ListnewsView extends GetView<ListnewsController> {
  const ListnewsView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   backgroundColor: Colors.white,
      //   leading: const Text(
      //     "CNBC",
      //     style: TextStyle(
      //         fontSize: 20, fontWeight: FontWeight.bold, color: Colors.black),
      //   ),
      //   centerTitle: true,
      //   actions: [
      //     const ChangeThemButtonWidget(),
      //     IconButton(
      //       icon: const Icon(
      //         Icons.menu,
      //         color: Colors.black,
      //       ),
      //       onPressed: () {},
      //     ),
      //     // add more IconButton
      //   ],
      // ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(
              height: 35,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Text(
                  "CNBC",
                  style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      color: Colors.black),
                ),
                IconButton(
                  icon: const Icon(
                    Icons.menu,
                    color: Colors.black,
                  ),
                  onPressed: () {},
                ),
              ],
            ),
            const ChangeThemButtonWidget(),
            const Text(
              "Hey,Jon!",
              style: TextStyle(fontSize: 16),
            ),
            const SizedBox(
              height: 16,
            ),
            const Text(
              "Dicover Latest News",
              maxLines: 2,
              style: TextStyle(
                fontSize: 30,
                fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            TextField(
              decoration: InputDecoration(
                filled: true,
                fillColor: const Color(0xFFFFFFFF),
                isDense: true,
                contentPadding: const EdgeInsets.symmetric(horizontal: 15.0),
                /* -- Text and Icon -- */
                hintText: "search for news...",
                hintStyle: const TextStyle(
                  fontSize: 18,
                  color: Color(0xFFB3B1B1),
                ), // TextStyle
                suffixIcon: Container(
                  decoration: BoxDecoration(
                      color: Colors.red,
                      borderRadius: BorderRadius.circular(4)),
                  child: const Icon(
                    Icons.search,
                    size: 21,
                    color: Colors.white,
                  ),
                ), // Icon
              ),
            ),
            const SizedBox(
              height: 16,
            ),
            const SizedBox(
              height: 16,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                          color: Colors.grey.withOpacity(0.2),
                          borderRadius: BorderRadius.all(Radius.circular(25))),
                      child: const Icon(
                        Icons.directions_boat_filled_outlined,
                        size: 25,
                      ),
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    const Text(
                      "Politics",
                      style: TextStyle(fontSize: 12),
                    )
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      padding: EdgeInsets.all(4),
                      decoration: BoxDecoration(
                          color: Colors.grey.withOpacity(0.2),
                          borderRadius: BorderRadius.all(Radius.circular(25))),
                      child: const Icon(
                        Icons.local_movies_rounded,
                        size: 25,
                        color: Colors.grey,
                      ),
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    const Text(
                      "Movies",
                      style: TextStyle(fontSize: 12),
                    )
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      padding: EdgeInsets.all(4),
                      decoration: BoxDecoration(
                          color: Colors.grey.withOpacity(0.2),
                          borderRadius: BorderRadius.all(Radius.circular(25))),
                      child: const Icon(
                        Icons.carpenter_rounded,
                        color: Colors.grey,
                        size: 25,
                      ),
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    const Text(
                      "Crime",
                      style: TextStyle(fontSize: 12),
                    )
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      padding: EdgeInsets.all(4),
                      decoration: BoxDecoration(
                          color: Colors.grey.withOpacity(0.2),
                          borderRadius: BorderRadius.all(Radius.circular(25))),
                      child: const Icon(
                        Icons.sports_handball_rounded,
                        color: Colors.grey,
                        size: 25,
                      ),
                    ),
                    const SizedBox(
                      height: 12,
                    ),
                    const Text(
                      "Sport",
                      style: TextStyle(fontSize: 12),
                    )
                  ],
                ),
              ],
            ),
            const SizedBox(
              height: 16,
            ),
            Obx(
              (() => Expanded(
                    child: ListView.builder(
                      scrollDirection: Axis.vertical,
                      itemCount: controller.articalList.length,
                      itemBuilder: (context, index) {
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            InkWell(
                              onTap: () {
                                Get.toNamed(Routes.newsdetail,
                                    arguments: controller.articalList[index]);
                              },
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  CachedNetworkImage(
                                    imageUrl: controller
                                            .articalList[index].urlToImage ??
                                        '',
                                    height: 80,
                                    width: 70,
                                    fit: BoxFit.cover,
                                    placeholder: (context, url) =>
                                        const CircularProgressIndicator(),
                                    errorWidget: (context, url, error) =>
                                        const Icon(
                                      Icons.image_not_supported,
                                    ),
                                  ),

                                  // ClipRRect(
                                  //   borderRadius: BorderRadius.circular(10),
                                  //   child: Image.network(
                                  //     controller
                                  //         controller.articalList[index].urlToImage ?? '',
                                  //     height: 80,
                                  //     width: 70,
                                  //     fit: BoxFit.cover,
                                  //   ),
                                  // ),
                                  const SizedBox(
                                    width: 16,
                                  ),
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          controller.articalList[index].title ??
                                              '',
                                          style: const TextStyle(
                                              fontSize: 20,
                                              fontWeight: FontWeight.bold),
                                        ),
                                        const SizedBox(
                                          height: 16,
                                        ),
                                        Text(controller
                                            .articalList[index].publishedAt
                                            .toString()),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                            const SizedBox(
                              height: 16,
                            ),
                          ],
                        );
                      },
                    ),
                  )),
            ),
          ],
        ),
      ),
    );
  }
}
