import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../../util/my_theme.dart';

class ChangeThemButtonWidget extends StatelessWidget {
  const ChangeThemButtonWidget({super.key});

  @override
  Widget build(BuildContext context) {
    final themeProvider = Provider.of<ThemePrivider>(context, listen: false);
    return Switch.adaptive(
      value: themeProvider.isDarkMode,
      onChanged: (value) {
        final provider = Provider.of<ThemePrivider>(context, listen: false);
        provider.toggleTheme(value);
        ChangeNotifier();
      },
    );
  }
}
