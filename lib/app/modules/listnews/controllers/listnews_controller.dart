import 'package:get/get.dart';
import 'package:news_api/app/api_module/api_service.dart';
import '../../../model/news_model.dart';

class ListnewsController extends GetxController {
  var articalList = <Article>[].obs;

  void fetcheNewsData() async {
    var newsData = await ApiService.getRequest();

    if (newsData != null) {
      // newsList.value = newsData;
      final List<Article> list = newsData.articles ?? [];
      articalList.value = list;
      print(articalList[0]);
    }
  }


  final count = 0.obs;
  @override
  void onInit() {
    super.onInit();
    fetcheNewsData();
    
  }
  // in this method calling insertdata method from Helper class

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  void increment() => count.value++;
}
