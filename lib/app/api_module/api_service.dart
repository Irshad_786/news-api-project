import 'package:http/http.dart' as http;
import 'package:news_api/app/model/news_model.dart';

class ApiService {
  static var client = http.Client();
  static Future<NewsModel> getRequest() async {
    String baseUrl =
        'https://newsapi.org/v2/everything?q=apple&from=2022-10-09&to=2022-10-09&sortBy=popularity&apiKey=e12cdc729128463fbcca83847dc3a876';

    var response = await client.get(Uri.parse(baseUrl), headers: {});
    if (response.statusCode == 200) {
      var jsonString = response.body;

      return newsModelFromJson(jsonString);
    } else {
      return null!;
    }
  }
}
