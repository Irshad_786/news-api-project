import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:news_api/util/my_theme.dart';
import 'package:news_api/util/my_theme.dart';
import 'package:provider/provider.dart';

import 'app/routes/app_pages.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) => ChangeNotifierProvider(
        create: (context) => ThemePrivider(),
        builder: (context, _) {
          final themePrivider = Provider.of<ThemePrivider>(context);
          return GetMaterialApp(
            debugShowCheckedModeBanner: false,
            title: 'Flutter Demo',
            themeMode: themePrivider.themeMode,
            darkTheme: MyThemes.darkTheme,
            theme: MyThemes.lightTheme,
            initialRoute: AppPages.initial,
            getPages: AppPages.routes,

            //  home: const MyHomePage(title: 'Flutter Demo Home Page'),
          );
        },
      );
}
