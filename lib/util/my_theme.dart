import 'package:flutter/material.dart';

class ThemePrivider extends ChangeNotifier {
  ThemeMode themeMode = ThemeMode.dark;
  bool get isDarkMode => themeMode == ThemeMode.dark;
  void toggleTheme(bool inOn) {
    themeMode = inOn ? ThemeMode.dark : ThemeMode.light;
    notifyListeners();
  }
}

class MyThemes {
  static final darkTheme = ThemeData(
      scaffoldBackgroundColor: Colors.grey.shade900,
      primaryColor: Colors.black,
      iconTheme: IconThemeData(color: Colors.purple.shade200, opacity: 0.8),
      colorScheme:
          const ColorScheme.dark() // Making All Text White in dark mode
      );
  static final lightTheme = ThemeData(
    scaffoldBackgroundColor: Colors.white,
    primaryColor: Colors.white,
    iconTheme: const IconThemeData(color: Colors.red, opacity: 0.8),
    colorScheme:
        const ColorScheme.light(), // Making All Text Black in light mode
  );
}
