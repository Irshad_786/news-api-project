// import 'package:sqflite/sqflite.dart';
// import 'package:path_provider/path_provider.dart';
// import 'dart:io' as io;
// import 'package:path/path.dart';
// import 'dart:async';

// import '../app/model/sqlflite_news_model.dart';
// class DbHelper {
//   static Database? _db;
//   Future<Database?> get db async {
//     if (_db != null) {
//       return _db;
//     }
//     _db =await initDatabase();
//      return _db;
//   }
//   initDatabase() async {
//    io.Directory documentDirectory=await getApplicationDocumentsDirectory();
//    String path =join(documentDirectory.path,'news.db');
//    var db =await openDatabase(path,version: 1,onCreate: _onCreate);
//    return db;
//   }
//   _onCreate(Database db, int version)async{
//    await db.execute("""
//             CREATE TABLE USERDETAILS(
//               id INTEGER PRIMARY KEY AUTOINCREMENT,
//               source TEXT  NOT NULL,
//               author TEXT  NOT NULL,
//               title TEXT  NOT NULL UNIQUE,
//               url TEXT  NOT NULL UNIQUE,
//               urlToImage TEXT  NOT NULL,
//               publishedAt TEXT  NOT NULL
//               content TEXT  NOT NULL
//             )

//           """);

//   }
//    Future<Article> insertData(Article article) async {

//     final Database db = await initDB();
//     db.insert("USERDETAILS",  article.toJson(),
//         conflictAlgorithm: ConflictAlgorithm.replace);

//     return true;
//   }

// }

import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:io' as io;
import 'package:path/path.dart';
import 'dart:async';
import '../app/model/news_model.dart';

class DataBaseHelper {
  // static DataBaseHelper _dataBaseHelper; //singletone DataBaseHelper
  // // Source? source ='';
  // String? author = 'author';
  // String? title = 'title';
  // String? description = 'description';
  // String? url = 'url';
  // String? urlToImage = 'urlToImage';
  // // DateTime? publishedAt ='';
  // String? content;
  // DataBaseHelper._createInstance(); //named Constuctor to instance of data base
  //factory DataBaseHelper() {
  //   // ignore: unnecessary_null_comparison, prefer_conditional_assignment
  //   if (_dataBaseHelper == null) {
  //     _dataBaseHelper =
  //         DataBaseHelper._createInstance(); // this will exicuteonce singletone
  //   }

  //   return _dataBaseHelper;
  // }

  static Database? _db;
  Future<Database?> get db async {
    if (_db != null) {
      return _db;
    }
    _db = await initDatabase();
    return _db;
  }

  initDatabase() async {
    io.Directory documentDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentDirectory.path, 'news.db');
    var db = await openDatabase(path, version: 1, onCreate: _onCreate);
    return db;
  }

  _onCreate(Database db, int version) async {
    await db.execute("""
            CREATE TABLE USERDETAILS(
              id INTEGER PRIMARY KEY AUTOINCREMENT,
              source TEXT  NOT NULL,
              author TEXT  NOT NULL,
              title TEXT  NOT NULL UNIQUE,
              url TEXT  NOT NULL UNIQUE,
              urlToImage TEXT  NOT NULL,
              publishedAt TEXT  NOT NULL
              content TEXT  NOT NULL
            )

          """);
  }

  Future<Database> initDB() async {
    String path = await getDatabasesPath();
    return openDatabase(
      join(path, "MyDB.db"),
      onCreate: (database, version) async {
        await database.execute("""
            CREATE TABLE USERDETAILS(
             id INTEGER PRIMARY KEY AUTOINCREMENT,
              source TEXT  NOT NULL,
              author TEXT  NOT NULL,
              title TEXT  NOT NULL UNIQUE,
              url TEXT  NOT NULL UNIQUE,
              urlToImage TEXT  NOT NULL,
              publishedAt TEXT  NOT NULL
              content TEXT  NOT NULL
            )

          """);
      },
      version: 1,
    );
  }

  Future<bool> insertData(Article article) async {
    final Database db = await initDB();
    db.insert("USERDETAILS", article.toJson(),
        conflictAlgorithm: ConflictAlgorithm.replace);

    return true;
  }

  Future<List> getData() async {
    final Database db = await initDB();
    final List<Map<String, Object?>> getdatas = await db.query("USERDETAILS");
    // final  getdatas= await db.query("USERDETAILS");
    return getdatas.map((e) => Article.fromJson(e)).toList();
  }

  Future<void> update(Article userupdate, int id) async {
    final Database db = await initDB();
    await db.update("USERDETAILS", userupdate.toJson(),
        where: "id=?", whereArgs: [id]);
  }

  Future<void> delete() async {
    final Database db = await initDB();
    await db.delete("USERDETAILS");
    //  where: "id=?", whereArgs: [id]);
  }
}
